angular.module('ionicApp.controllers', ['ionic.contrib.ui.tinderCards', 'monospaced.elastic'])

.controller('NavCtrl', function($scope, $ionicSideMenuDelegate, $state, Auth, Profiles, ChatList, TempMatch, Messages) {
  $scope.showMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
  $scope.showRightMenu = function () {
    $ionicSideMenuDelegate.toggleRight();
    $scope.reset();
  };
  $scope.loggedIn = false;
  $scope.auth = Auth;
  $scope.profiles = Profiles;
  $scope.auth.$onAuth(function(authData) {
    if (authData) {
      $scope.loggedIn = true;
      $scope.profiles.$loaded().then(function(){
          // if (!$scope.profiles[authData.facebook.id].hasOwnProperty("location")) {
          //     $state.go("location");
          // }
      })
      $scope.tempmatch = TempMatch;
      $scope.chatmates = ChatList.get(Auth.$getAuth().facebook.id);
      $scope.messagecount = [];
      $scope.lastMessage = {};
      $scope.hasNewMessage = false;
      $scope.chatlist = {
        temp: [],
        friends: []
      };
      var resetChatList = function(e) {
          $scope.chatlist = {
            temp: [],
            friends: []
          };
          var chatmatelist = Object.keys($scope.chatmates).map(function(chatmateKey, index){
            return $scope.chatmates[chatmateKey];
          });
          $scope.tempmatch[Auth.$getAuth().facebook.id].forEach(function(id){
            if (chatmatelist.indexOf(id) > -1 ) {
              $scope.chatlist.friends.push(id);
            } else {
              $scope.chatlist.temp.push(id);
            }
          });
          $scope.lastMessage = $scope.tempmatch[Auth.$getAuth().facebook.id].map(function(chatmate, index){
            // var chatmate = $scope.chatmates[chatmateKey];
            var msg = Messages.lastMessage(chatmate, Auth.$getAuth().facebook.id);
            msg.$loaded().then(function(){
                $scope.lastMessage[chatmate] = msg[0];
            });
            msg.$watch(function(){
              $scope.lastMessage[chatmate] = msg[0];
            })
          });
          $scope.messagelist = $scope.tempmatch[Auth.$getAuth().facebook.id].map(function(chatmate, index){
              // var chatmate = $scope.chatmates[chatmateKey];
              var msgs = Messages.forUsers(chatmate, Auth.$getAuth().facebook.id);
              $scope.messagecount.push(0);
              msgs.$loaded().then(function(){
                  msgs.$watch(function(e){
                      if(e.event === "child_added") {
                          console.log(e);
                          var rec = msgs.$getRecord(e.key);
                          $scope.lastMessage[chatmate] = rec;
                            if (rec.from !== Auth.$getAuth().facebook.id) {
                                $scope.messagecount[index] += 1;
                                $scope.hasNewMessage = true;
                            }    
                         
                          
                      }
                  });
              });
              return msgs;
          });
          console.log($scope.messagecount, $scope.messagelist);    
      }
      $scope.tempmatch.$watch(resetChatList);
      $scope.chatmates.$watch(resetChatList);
      $scope.chatmates.$loaded().then(function(){
          resetChatList();
          
      });
      
      $scope.reset = function() {
          $scope.messagecount = $scope.messagecount.map(function(n){
              return 0;
          });
          $scope.hasNewMessage = false;
      };
      $scope.reduce = function(x) {
          return x.reduce(function(e, y){return e + y;}, 0);
      }
      
      $scope.resetIndex = function(index) {
          $scope.hasNewMessage = false;
          $scope.messagecount[index] = 0;
      }
      $scope.searchIndex = function(id) {
          return $scope.tempmatch[Auth.$getAuth().facebook.id].indexOf(id);
      }
    }
    
  });
})
.controller("LocationCtrl", function($scope, $state, Places, Profiles, Auth){
    var profiles = Profiles,
        user_id = Auth.$getAuth().facebook.id;
    $scope.location = {text:""};
    if(profiles[user_id].hasOwnProperty("location")) {
        $scope.location.text = profiles[user_id].location;
    }
    $scope.getPlaces = function(query) {
        return Places.filter(query);
    };
    $scope.saveLocation = function() {
        profiles[user_id].location = $scope.location.text;
        profiles.$save().then(function(){
            $state.go("tabs.explore");
        });
    };
})
.controller('OffersTabCtrl', function($scope, $ionicLoading, Offers, Auth, Profiles, ChatMates, Messages) {
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>'
    });
    $scope.currentUserId = Auth.$getAuth().facebook.id
    $scope.offersArr = Offers.userOffers();
    $scope.user_id = Auth.$getAuth().facebook.id;
    $scope.offers = [];
    $scope.offersArr[1].$loaded().then(function(){
        for(var i=0; i<$scope.offersArr.length; i++) {
            $scope.offersArr[i] = $scope.offersArr[i].map(function(offer){
                offer.ref = $scope.offersArr[i].$keyAt(offer)
                return offer;
            });
        }
        $scope.offers = $scope.offersArr[0].concat($scope.offersArr[1]);
        $ionicLoading.hide();
    });
    $scope.profiles = Profiles;
    $scope.acceptOffer = function(uid1, uid2, offerRef, index) {
        var offer = Offers.get(offerRef);
        var chatmates = ChatMates;
        var doesNotContain = function(arr, uid) {
            var x = arr;
            return x.filter(function(id){
                return id === uid;
            }) === 0;
        }
        offer.$loaded().then(function(){
            offer.status = "accepted";
            offer.$save();
            chatmates.$loaded().then(function(){
                if (!chatmates.hasOwnProperty(offer.uid1)) {
                    chatmates[offer.uid1] = [];
                }
                if (!chatmates.hasOwnProperty(offer.uid2)) {
                    chatmates[offer.uid2] = [];
                }
                if (doesNotContain(chatmates[offer.uid1], uid2)) {
                    chatmates[offer.uid1].push(offer.uid2);    
                }
                if (doesNotContain(chatmates[offer.uid2], uid1)) {
                    chatmates[offer.uid2].push(offer.uid1);    
                }
                chatmates.$save().then(function(){
                    var messages = Messages.forUsers(offer.uid1, offer.uid2);
                    messages.$loaded().then(function(){
                        messages.$add({
                            from: Auth.$getAuth().facebook.id,
                            message: "Hey, let's Bartr",
                            timestamp: Firebase.ServerValue.TIMESTAMP
                        });
                        $scope.offers.splice(index, 1);
                    });
                });
            });
        });
    };
    $scope.declineOffer = function(offerRef, index) {
        var offer = Offers.get(offerRef)
        offer.$loaded().then(function(){
            offer.status = "rejected";
            offer.$save();
            $scope.offers.splice(index, 1);
        });
    };
})
.controller('MatchTabCtrl', function($scope, $q, $state, $firebaseArray, $ionicLoading, $ionicListDelegate, $ionicPopup, $ionicModal, Books, Auth, Matches, BookCache) {
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>'
    });
    $scope.bookQ = ""
    $scope.tradeBooks = {query:""};
    $scope.tradeBooksList = [];
    $scope.selectedBook = {};
    $scope.books = Books.userBooks();
    $scope.books.$loaded().then(function(){
        $ionicLoading.hide();
    });
    $ionicModal.fromTemplateUrl('templates/matchmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.book = {};
    $scope.bookIndex = 0;

    $ionicModal.fromTemplateUrl('templates/bookModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.bookModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/bookDelete.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.bookDeleteModal = modal;
    });

    var onLoad = function() {
      $scope.talkBookList = $scope.userMatches.map(function(match){
        return $scope.bookCache[match.bookId];
      });
    }

    $scope.talkBookList = [];
    $scope.userMatches = Matches.userMatches();
    $scope.bookCache = BookCache;
    $scope.userMatches.$loaded().then(function(){
      $scope.bookCache.$loaded().then(onLoad);
      $scope.bookCache.$watch(onLoad);
      $scope.userMatches.$watch(onLoad);
      // var deferredMap = $scope.userMatches.map(function(match){
      //   var dfd = $q.defer()
      //   gapi.client.books.volumes.get({volumeId: match.bookId}).execute(function(item){
      //      dfd.resolve({
      //           id: item.id,
      //           name: item.volumeInfo.title,
      //           description: item.volumeInfo.description || '',
      //           imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
      //           categories: item.volumeInfo.categories || [],
      //           authors: item.volumeInfo.authors || []
      //       });

      //   });
      //   return dfd.promise;
      // });
      // $q.all(deferredMap).then(function(values){
      //   $scope.talkBookList = values;
      // });
    });


    $scope.openMatchModal = function() {
      $scope.modal.show();
    }

    $scope.closeMatchModal = function() {
      $scope.modal.hide();
    }

    $scope.openBookModal = function(index) {
      $scope.modal.hide();
      $scope.bookIndex = index;
      $scope.book = $scope.tradeBooksList[index];
      $scope.bookModal.show();
    }

    $scope.closeBookModal = function() {
      $scope.bookModal.hide();
      $scope.modal.show();

    }

    $scope.closeBookDeleteModal = function() {
      $scope.bookDeleteModal.hide();
    }

    $scope.bookCache.$watch(function(){
      $scope.bookCache.$loaded().then(function(){
        $scope.talkBookList = $scope.userMatches.map(function(match){
          return $scope.bookCache[match.bookId];
        });
      });
    });

    $scope.userMatches.$watch(function(){
      $scope.bookCache.$loaded().then(function(){
        $scope.talkBookList = $scope.userMatches.map(function(match){
          return $scope.bookCache[match.bookId];
        });
      });
    //   var deferredMap = $scope.userMatches.map(function(match){
    //     var dfd = $q.defer()
    //     gapi.client.books.volumes.get({volumeId: match.bookId}).execute(function(item){
    //        dfd.resolve({
    //             id: item.id,
    //             name: item.volumeInfo.title,
    //             description: item.volumeInfo.description || '',
    //             imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
    //             categories: item.volumeInfo.categories || [],
    //             authors: item.volumeInfo.authors || []
    //         });

    //     });
    //     return dfd.promise;
    //   });
    //   $q.all(deferredMap).then(function(values){
    //     $scope.talkBookList = values;
    //   });
    })

    $scope.getBooks = function(query) {
        var dfd = $q.defer();

        if (query) {
            
            gapi.client.books.volumes.list({q:query}).execute(function(res){
              var items = res.items.map(function(item, index){
                      return {
                          id: item.id,
                          name: item.volumeInfo.title,
                          description: item.volumeInfo.description || '',
                          imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
                          categories: item.volumeInfo.categories || [],
                          authors: item.volumeInfo.authors || []
                      }
                  });
                dfd.resolve(items);
            });
            
            
        } else {
          dfd.resolve([]);
        }
        return dfd.promise;
        
    }; 
    $scope.getTradeBook = function(callback) {
        $scope.tradeBooksList.push(callback.item);
    };
    $scope.removeTradeBook = function(callback) {
        $scope.tradeBooksList = $scope.tradeBooksList.filter(function(item){
            return item.id !== callback.item.id;
        })
    };
    $scope.checkBook = function(book) {
        var obj = book;
        obj.ref = $scope.books.$keyAt(book);
        $state.go("tabs.exploreList", {selectedBook: obj});
    };
    $scope.deleteBook = function(book) {
        $scope.books.$remove(book).then(function(){
            
        });
    } 
    $scope.tradeBooksQuery = function() {
      $scope.getBooks($scope.tradeBooks.query).then(function(items){
        $scope.tradeBooksList = items;
      });
    }

    $scope.addTalkBookList = function(index) {
      if ($scope.talkBookList.length < 5) {
        // $scope.talkBookList.push($scope.tradeBooksList[index]);
        // $scope.tradeBooksList.splice(index, 1);
        $ionicListDelegate.closeOptionButtons();
        $scope.modal.hide();
        Matches.all().$add({
          bookId: $scope.tradeBooksList[index].id,
          uid: Auth.$getAuth().facebook.id
        }, function(ref){
          console.log(ref);

        });
        $scope.bookCache[$scope.tradeBooksList[index].id] = $scope.tradeBooksList[index];
        $scope.bookCache.$save().then(function(ref){
          console.log(ref);
        });
      }
    }

    $scope.chooseBook = function(index) {
      if ($scope.talkBookList.length < 5) {
        // $scope.talkBookList.push($scope.tradeBooksList[index]);
        // $scope.tradeBooksList.splice(index, 1);
        $ionicListDelegate.closeOptionButtons();
        $scope.modal.hide();
        $scope.bookModal.hide();
        Matches.all().$add({
          bookId: $scope.tradeBooksList[index].id,
          uid: Auth.$getAuth().facebook.id
        }, function(ref){
          console.log(ref);

        });
        $scope.bookCache[$scope.tradeBooksList[index].id] = $scope.tradeBooksList[index];
        $scope.bookCache.$save().then(function(ref){
          console.log(ref);
        });
      }
    }

    $scope.viewBook = function(index) {
      if (($scope.talkBookList.length > index) && ($scope.talkBookList.length > 0)) {
          $scope.book = $scope.talkBookList[index];
          $scope.bookIndex = index;
          $scope.bookDeleteModal.show();
        }
    }

    $scope.removeTalkBookList = function(index) {
       if ($scope.talkBookList.length > index) {
          var confirmPopup = $ionicPopup.confirm({
             title: 'Remove from List',
             template: 'Are you sure you want to remove the book from this list?'
           });
          confirmPopup.then(function(res) {
             if(res) {
               console.log('You are sure');
                  // $scope.talkBookList.splice(index, 1);
                  $scope.userMatches.$remove(index);
                  $scope.bookDeleteModal.hide();
             } else {
               console.log('You are not sure');
             }
           });

        }
      
    }

    $scope.submitBooks = function() {
      var obj = {books: []}
      $scope.talkBookList.forEach(function(el){
        obj.books.push(el)
        
      });
      
      $state.go("tabs.matchList", {selectedBook: obj});
    }
})
.controller('MatchListTabCtrl', function($scope, $stateParams, $ionicLoading, $state, Books, Profiles, Offers, Messages, Auth, Matches, TempMatch, ChatList) {
    $scope.selectedBook = $stateParams.selectedBook.books;
    $scope.userId = Auth.$getAuth().facebook.id;
    $scope.bookMatches = $scope.selectedBook.map(function(book){
      var obj = book;
      book.matches = Matches.bookMatches(book.id)
      // .filter(function(matches){
      //   return matches.uid !== $scope.userId;
      // })
      return book;
    });
    $scope.chatmates = ChatList.get($scope.userId);
    $scope.userBookMatches = {}
    $scope.selectedBook.forEach(function(book){
        var obj = book;
        var matches = Matches.bookMatches(book.id);
        matches.$loaded().then(function(){
            matches.forEach(function(match){
                if(match.uid != $scope.userId) {
                    if (!$scope.userBookMatches.hasOwnProperty(match.uid)) {
                        $scope.userBookMatches[match.uid] = [];
                    }
                    $scope.userBookMatches[match.uid].push(obj);    
                }
                
            });  
        });
        var bookInArray = function(oldBook, matches) {
          return matches.filter(function(book){
            return book.id == oldBook.id; 
          }).length > 0;
        };
        matches.$watch(function(){
          matches.forEach(function(match){
                if(match.uid != $scope.userId) {
                    if (!$scope.userBookMatches.hasOwnProperty(match.uid)) {
                        $scope.userBookMatches[match.uid] = [];
                    }
                    if ((!bookInArray(obj, $scope.userBookMatches[match.uid])) && (match.bookId !== obj.id)) {
                      $scope.userBookMatches[match.uid].push(obj);    
                    }
                }
                
            });
        })
          // .filter(function(matches){
          //   return matches.uid !== $scope.userId;
          // })
    });
    $scope.profiles = Profiles;
    $scope.isFriend = function(uid) {
      return Object.keys($scope.chatmates).map(function(key){
        return $scope.chatmates[key];
      }).indexOf(uid) > -1;
    }
    $scope.addTempMatch = function(uid) {
      var doesNotContain = function(arr, uid) {
          var x = arr;
          return x.filter(function(id){
              return id === uid;
          }).length === 0;
      };
      var chatmates = TempMatch;
      chatmates.$loaded().then(function(){
          if (!chatmates.hasOwnProperty(uid)) {
              chatmates[uid] = [];
          }
          if (!chatmates.hasOwnProperty($scope.userId)) {
              chatmates[$scope.userId] = [];
          }
          if (doesNotContain(chatmates[uid], $scope.userId)) {
              chatmates[uid].push($scope.userId);
          }
          if (doesNotContain(chatmates[$scope.userId], uid)) {
              chatmates[$scope.userId].push(uid);
          }
          chatmates.$save().then(function(){
              $state.go("tabs.tempchat", {id: uid});
          });
      });  
    }
    
})
.controller("UserTabCtrl", function($scope, $ionicModal, $state, Auth, Profiles, Matches, BookCache, ChatList){
  $scope.profile = Profiles;
  $scope.userId = Auth.$getAuth().facebook.id;
  $scope.chatmates = ChatList.get($scope.userId);
  $scope.quote = $scope.profile[$scope.userId].quote || {message: "", character: "", book: ""}; 
  $ionicModal.fromTemplateUrl('templates/quote.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.editQuote = function() {
    $scope.modal.show();
  };

  $scope.saveQuote = function() {
    $scope.profile[$scope.userId].quote = $scope.quote;
    $scope.profile.$save().then(function(){
      $scope.modal.hide();  
    });
  }

  $ionicModal.fromTemplateUrl('templates/bookDetail.html', {
      scope: $scope,
      animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.bookDetailModal = modal;
  });

  $scope.talkBookList = []  
  $scope.bookCache = BookCache;
  $scope.userMatches = Matches.userMatches();

  var onLoad = function() {
    $scope.talkBookList = $scope.userMatches.map(function(match){
        return $scope.bookCache[match.bookId];
      });
  }

  $scope.userMatches.$watch(onLoad);
  $scope.bookCache.$watch(onLoad);

  $scope.userMatches.$loaded().then(function(){
    $scope.bookCache.$loaded().then(function(){
      onLoad(); 
    });
    // var deferredMap = $scope.userMatches.map(function(match){
    //   var dfd = $q.defer()
    //   gapi.client.books.volumes.get({volumeId: match.bookId}).execute(function(item){
    //      dfd.resolve({
    //           id: item.id,
    //           name: item.volumeInfo.title,
    //           description: item.volumeInfo.description || '',
    //           imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
    //           categories: item.volumeInfo.categories || [],
    //           authors: item.volumeInfo.authors || []
    //       });

    //   });
    //   return dfd.promise;
    // });
    // $q.all(deferredMap).then(function(values){
    //   $scope.talkBookList = values;
    // });
  });
  $scope.viewFriend = function(uid) {
    if (uid) {
      $state.go("tabs.friend", {id:uid});
    }
  }

  $scope.viewBook = function(index) {
    if (($scope.talkBookList.length > index) && ($scope.talkBookList.length > 0)) { 
      $scope.book = $scope.talkBookList[index];
      $scope.bookIndex = index;
      $scope.bookDetailModal.show();
    }
  }

  $scope.closeBookDetailModal = function() {
    $scope.bookDetailModal.hide();
  }

})
.controller("FriendsTabCtrl", function($scope, $state, Auth, ChatList, Profiles, BookCache, Matches) {
  $scope.bookCache = BookCache;
  $scope.chatmates = ChatList.get(Auth.$getAuth().facebook.id);
  $scope.profile = Profiles
  $scope.matches = {};
  var loadMatches =  function() {
    Object.keys($scope.chatmates).map(function(key){
      return $scope.chatmates[key];
    }).forEach(function(chatmate){
      $scope.matches[chatmate] = Matches.friendMatches(chatmate);
    });
  }
  $scope.chatmates.$loaded().then(loadMatches);
  $scope.isObjectEmpty = function(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
  }
  $scope.viewFriend = function(uid) {
    if (uid) {
      $state.go("tabs.friend", {id:uid});
    }
  }
})
.controller("FriendTabCtrl", function($scope, $stateParams, $q, $state, $ionicModal, Profiles, Matches, BookCache, ChatList, Auth, Messages){
  $scope.userId = $stateParams.id;
  $scope.profile = Profiles;
  $scope.talkBookList = [] 
  $scope.bookCache = BookCache 
  $scope.chatmates = ChatList.get($scope.userId);
  $scope.userMatches = Matches.friendMatches($scope.userId);
  $scope.tradeBooks = {query: ''};

  $ionicModal.fromTemplateUrl('templates/bookDetail.html', {
      scope: $scope,
      animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.bookDetailModal = modal;
  });

  $ionicModal.fromTemplateUrl('templates/matchmodal.html', {
      scope: $scope,
      animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.modal = modal;
  });

  $scope.book = {};
  $scope.bookIndex = 0;

  $ionicModal.fromTemplateUrl('templates/bookModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.bookModal = modal;
  });

  var onLoad = function() {
    $scope.talkBookList = $scope.userMatches.map(function(match){
        return $scope.bookCache[match.bookId];
      });
  }

  $scope.userMatches.$watch(onLoad);
  $scope.bookCache.$watch(onLoad);

  $scope.userMatches.$loaded().then(function(){
    $scope.bookCache.$loaded().then(function(){
      onLoad();
    });
    // var deferredMap = $scope.userMatches.map(function(match){
    //   var dfd = $q.defer()
    //   gapi.client.books.volumes.get({volumeId: match.bookId}).execute(function(item){
    //      dfd.resolve({
    //           id: item.id,
    //           name: item.volumeInfo.title,
    //           description: item.volumeInfo.description || '',
    //           imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
    //           categories: item.volumeInfo.categories || [],
    //           authors: item.volumeInfo.authors || []
    //       });

    //   });
    //   return dfd.promise;
    // });
    // $q.all(deferredMap).then(function(values){
    //   $scope.talkBookList = values;
    // });
  });
  $scope.viewFriend = function(uid) {
    if (uid) {
      if (uid === Auth.$getAuth().facebook.id) {
        $state.go("tabs.user")
      } else {
        $state.go("tabs.friend", {id:uid});
      }
    }
    
  };

  $scope.getBooks = function(query) {
        var dfd = $q.defer();

        if (query) {
            
            gapi.client.books.volumes.list({q:query}).execute(function(res){
              var items = res.items.map(function(item, index){
                      return {
                          id: item.id,
                          name: item.volumeInfo.title,
                          description: item.volumeInfo.description || '',
                          imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
                          categories: item.volumeInfo.categories || [],
                          authors: item.volumeInfo.authors || []
                      }
                  });
                dfd.resolve(items);
            });
            
            
        } else {
          dfd.resolve([]);
        }
        return dfd.promise;
        
    }; 

    $scope.tradeBooksQuery = function() {
      $scope.getBooks($scope.tradeBooks.query).then(function(items){
        $scope.tradeBooksList = items;
      });
    };


    $scope.viewBook = function(index) {
      if (($scope.talkBookList.length > index) && ($scope.talkBookList.length > 0)) {
        $scope.book = $scope.talkBookList[index];
        $scope.bookIndex = index;
        $scope.bookDetailModal.show();
      }
    }

    $scope.openMatchModal = function() {
      $scope.modal.show();
    }

    $scope.closeMatchModal = function() {
      $scope.modal.hide();
    }

    $scope.openBookModal = function(index) {
      $scope.modal.hide();
      $scope.bookIndex = index;
      $scope.book = $scope.tradeBooksList[index];
      $scope.bookModal.show();
    }

    $scope.closeBookModal = function() {
      $scope.bookModal.hide();
      $scope.modal.show();

    }

  $scope.closeBookDetailModal = function() {
    $scope.bookDetailModal.hide();
  }

  $scope.referBook = function() {
    $scope.modal.show();
  }

  $scope.chooseBook = function(bookIndex) {
    var messages = Messages.forUsers(Auth.$getAuth().facebook.id, $scope.userId);
    var book = $scope.book;
    var msg = "Hey, check out this book: \n";
        msg += book.name;
        msg += book.authors.length > 0 ? " by " + book.authors.join(", ") + "\n" : "\n";
        msg += book.description;
    messages.$loaded().then(function(){
        messages.$add({
            from: Auth.$getAuth().facebook.id,
            message: msg,
            timestamp: Firebase.ServerValue.TIMESTAMP
        });
        $scope.bookModal.hide();
        $state.go('chat', {id: $scope.userId});
     });

  }
})
.controller('ExploreTabCtrl', function($scope, $q, $state, $firebaseArray, $ionicLoading, Books, Auth, Camera) {
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>'
    });
    $scope.bookQ = ""
    $scope.tradeBooksQ = "";
    $scope.tradeBooksList = [];
    $scope.selectedBook = {};
    $scope.books = Books.userBooks();
    $scope.books.$loaded().then(function(){
        $ionicLoading.hide();
    })
    $scope.image = "";
    $scope.takePicture = function() {
        if(ionic.Platform.isWebView()){
          Camera.getPicture({
            destinationType: navigator.camera.DestinationType.DATA_URL
          }).then(function(imageURI) {
            console.log(imageURI)
            $scope.image = "data:image/png;base64," + imageURI;
          }, function(err) {
            console.log(err);
          });  
        } else {
          $scope.image = "http://placehold.it/250x250"  
        }
        
        
    }
    $scope.getBooks = function(query) {
        if (query) {
            var dfd = $q.defer();
            
            gapi.client.books.volumes.list({q:query}).execute(function(res){
              var items = res.items.map(function(item, index){
                      return {
                          id: item.id,
                          name: item.volumeInfo.title,
                          description: item.volumeInfo.description || '',
                          imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
                          categories: item.volumeInfo.categories || [],
                          authors: item.volumeInfo.authors || []
                      }
                  });
                dfd.resolve(items);
            });
            return dfd.promise;
            
        }
        return [];
    }; 
    $scope.getBook = function(callback) {
        $scope.selectedBook = callback.item;
    };
    $scope.removeBook = function(callback) {
        $scope.selectedBook = {};
    }
    $scope.getTradeBook = function(callback) {
        $scope.tradeBooksList.push(callback.item);
    };
    $scope.removeTradeBook = function(callback) {
        $scope.tradeBooksList = $scope.tradeBooksList.filter(function(item){
            return item.id !== callback.item.id;
        })
    };
    $scope.submitBooks = function(callback) {
        var obj = $scope.selectedBook;
        obj.tradeBooksList = $scope.tradeBooksList;
        obj.user_id = Auth.$getAuth().facebook.id;
        obj.image = $scope.image;
        Books.all().$add(obj).then(function(ref){
            obj.ref = ref.key();
        });
        $state.go("tabs.exploreList", {selectedBook: obj});
    };
    $scope.checkBook = function(book) {
        var obj = book;
        obj.ref = $scope.books.$keyAt(book);
        $state.go("tabs.exploreList", {selectedBook: obj});
    };
    $scope.deleteBook = function(book) {
        $scope.books.$remove(book).then(function(){
            
        });
    } 
})
.controller('ExploreListTabCtrl', function($scope, $stateParams, $ionicLoading, Books, Profiles, Offers, Messages, Auth) {
    $scope.selectedBook = $stateParams.selectedBook;
    $scope.tradeBooks = Books.searchBooks($scope.selectedBook);
    
    if ($scope.tradeBooks.length > 0) {
        $ionicLoading.show({
          template: '<ion-spinner icon="ripple"></ion-spinner>'
        });
        $scope.tradeBooks[0].query.$loaded().then(function(){
            $ionicLoading.hide();
        })
    }
    $scope.deleteBook = function(index) {
        alert(index);
    };
    $scope.makeOffer = function(uid, book, parentIndex, index) {
        var user_id = Auth.$getAuth().facebook.id,
            obj = {};
        obj.uid1 = user_id;
        obj.uid2 = uid;
        obj.book1 = $scope.selectedBook;
        obj.book2 = book;
        obj.ref1 = obj.book1.ref;
        obj.ref2 = obj.book2.ref;
        obj.status = "offer";
        obj.from = user_id;
        var book2Trade = book.tradeBooksList.map(function(b){
            return b.id;
        });
        if (book2Trade.indexOf(obj.book1.id) > -1) {
            obj.status = "accepted";
            var messages = Messages.forUsers(obj.uid1, obj.uid2);
            messages.$loaded().then(function(){
                messages.$add({
                    from: Auth.$getAuth().facebook.id,
                    message: "Hey, let's Bartr",
                    timestamp: Firebase.ServerValue.TIMESTAMP
                });
                $scope.tradeBooks[parentIndex].query.splice(index, 1);
            });
        } 
        Offers.all().$add(obj).then(function(){
            $scope.tradeBooks[parentIndex].query.splice(index, 1);
        });    
        
        
    }
})
.controller('MixAndMatchTabCtrl', function($scope, $ionicModal, removeOffersFilter, Books, Auth, Profiles, Offers, Messages) {
  var arrayObjectIndexOf = function(myArray, searchTerm, property) {
    for(var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
    }
    return -1;
  }
  $scope.books = Books.all(); 
  $scope.userBooks = Books.userBooks();
  $scope.userBookIndex = 0;
  $scope.currentUserId = Auth.$getAuth().facebook.id;
  $scope.profiles = Profiles;
  $scope.books.$loaded().then(function(){
    $scope.matchBooks = $scope.books.filter(function(book){
        return book.user_id !== $scope.currentUserId;
    })    
  });
  $scope.offers = Offers.userOffers();
  $ionicModal.fromTemplateUrl('templates/bookSelect.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.bookSelect = modal;
  });
  $scope.openBookSelect = function() {
    $scope.bookSelect.show();
  };
  $scope.closeBookSelect = function() {
    $scope.bookSelect.hide();
  };
  $scope.selectBook = function(index) {
      $scope.userBookIndex = index;
      $scope.bookSelect.hide();
  }
  $scope.cardDestroyed = function(card) {
    
  };

  $scope.addCard = function() {
    var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
    newCard.id = Math.random();
    $scope.cards.push(angular.extend({}, newCard));
  }
  $scope.onSwipedLeft = function(card) {
    
    $scope.matchBooks.splice(arrayObjectIndexOf($scope.matchBooks, card.id, "id"), 1);
  };
  $scope.onSwipedRight = function(card) {
     var user_id = Auth.$getAuth().facebook.id,
        obj = {};
    obj.uid1 = user_id;
    obj.uid2 = card.user_id;
    obj.book1 = $scope.userBooks[$scope.userBookIndex];
    obj.book2 = card;
    obj.ref1 = $scope.userBooks.$keyAt(obj.book1);
    obj.ref2 = $scope.books.$keyAt(obj.book2);
    obj.status = "offer";
    obj.from = user_id;
    var book2Trade = card.tradeBooksList.map(function(b){
        return b.id;
    });
    console.log(obj.ref1);
    if (book2Trade.indexOf(obj.book1.id) > -1) {
        obj.status = "accepted";
        var messages = Messages.forUsers(obj.uid1, obj.uid2);
        messages.$loaded().then(function(){
            messages.$add({
                from: Auth.$getAuth().facebook.id,
                message: "Hey, let's Bartr",
                timestamp: Firebase.ServerValue.TIMESTAMP
            });
        });
    } 
    Offers.all().$add(obj).then(function(){
        $scope.matchBooks.splice(arrayObjectIndexOf($scope.matchBooks, card.id, "id"), 1);
    });    
  };
})
.controller('CardCtrl', function($scope, TDCardDelegate, $ionicModal, $ionicLoading) {
  $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>'
    });
  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $ionicLoading.hide();
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  
  $scope.decline = function() {
      $scope.matchBooks.splice($scope.matchBooks.indexOf($scope.card), 1);
  };
  
  $scope.declineModal = function() {
      $scope.modal.hide()
      $scope.matchBooks.splice($scope.matchBooks.indexOf($scope.card), 1);
  };
}).controller("LoginCtrl", function($scope, $ionicLoading, Auth, $state, Profiles){
    $scope.login = function() {
      // Auth.$authWithOAuthRedirect("facebook").then(function(authData) {
      //     console.log("Logged in as:", authData.uid);
      //   });
      Auth.$authWithOAuthPopup('facebook')
        .then(function(authData) {
          $state.go('tabs.match');
        });

    };
    Auth.$onAuth(function(authData) {
      if (authData === null) {
        console.log("Not logged in yet");
      } else {
        console.log("Logged in as", authData.uid);
        Profiles.$loaded().then(function(){
            if(!Profiles.hasOwnProperty(authData.facebook.id)) {
                Profiles[authData.facebook.id] = authData.facebook;
                Profiles.$save().then(function(ref){
                    console.log(Profiles);
                });     
            }
               
        })
        
        $state.go("tabs.match")
      }
      $scope.authData = authData; // This will display the user's name in our view
    });
    $scope.logout = function() {
        Auth.$unauth();
        $state.go("login");
    };
})
.controller("ChatCtrl", function($scope, $stateParams, $ionicScrollDelegate, $focusTest, Profiles, Messages, Auth, Matches, BookCache){
    $scope.chatmateId = $stateParams.id;
    $scope.currentUserId = Auth.$getAuth().facebook.id;
    $scope.profiles = Profiles;
    $scope.messages = Messages.forUsers($scope.chatmateId, $scope.currentUserId);
    $scope.msg = {text: ""};
    $scope.bookCache = BookCache;
    $scope.userMatches = Matches.userMatches();
    $scope.friendMatches = Matches.friendMatches($scope.chatmateId);
    $scope.commonMatches = [];

    $scope.focusManager = { focusInputOnBlur: true};
    $scope.shouldNotFocusOnBlur = function() {
       $focusTest.setFocusOnBlur(false);
    };


    $scope.messages.$loaded().then(function(){

            $ionicScrollDelegate.scrollBottom();
            $ionicScrollDelegate.scrollBy(0, 9999999);        
    });
    $scope.friendMatches.$loaded().then(function(){
        $scope.friendMatches.forEach(function(friendMatch){
          $scope.userMatches.forEach(function(match){
            if(match.bookId === friendMatch.bookId) {
              $scope.commonMatches.push($scope.bookCache[friendMatch.bookId]);
            }
          })
        });
    });
    $scope.messages.$watch(function(e){
        if(e.event === "child_added") {
            $ionicScrollDelegate.scrollBottom();
            $ionicScrollDelegate.scrollBy(0, 9999999);
        }
    });
    $scope.sendMessage = function() {

        if ($scope.msg.text.length > 0) {
            $scope.messages.$add({
                from: $scope.currentUserId,
                message: $scope.msg.text,
                timestamp: Firebase.ServerValue.TIMESTAMP
            }).then(function(){
                $scope.msg.text = "";
            });
        }
        
    }
     $scope.$on('taResize', function(e, ta) {
      console.log('taResize');
      if (!ta) return;
      
      var taHeight = ta[0].offsetHeight;
      console.log('taHeight: ' + taHeight);
      
      if (!footerBar) return;
      
      var newFooterHeight = taHeight + 10;
      newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
      
      footerBar.style.height = newFooterHeight + 'px';
      scroller.style.bottom = newFooterHeight + 'px'; 
    });
         var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');

})
.controller("TempChatCtrl", function($scope, $stateParams, $timeout, $ionicScrollDelegate, $focusTest, Profiles, Messages, Auth, ChatMates, RejectMatch, Matches, BookCache, $state, $ionicHistory){
    $scope.chatmateId = $stateParams.id;
    $scope.currentUserId = Auth.$getAuth().facebook.id;
    $scope.userId = Auth.$getAuth().facebook.id;

    $scope.bookCache = BookCache;
    $scope.profiles = Profiles;
    $scope.messages = Messages.forUsers($scope.chatmateId, $scope.currentUserId);
    $scope.msg = {text: ""};
    $scope.userMatches = Matches.userMatches();
    $scope.friendMatches = Matches.friendMatches($scope.chatmateId);
    $scope.commonMatches = [];

    $scope.focusManager = { focusInputOnBlur: true};
    $scope.shouldNotFocusOnBlur = function() {
      $focusTest.setFocusOnBlur(false);
    };


    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
    var footerBar; // gets set in $ionicView.enter
    var scroller;
    var txtInput; // ^^^

    $scope.$on('$ionicView.enter', function() {
      console.log('UserMessages $ionicView.enter');
      
      $timeout(function() {
        footerBar = document.body.querySelector('#userMessagesView .bar-footer');
        scroller = document.body.querySelector('#userMessagesView .scroll-content');
        txtInput = angular.element(footerBar.querySelector('textarea'));
      }, 0);
    })

    $scope.messages.$loaded().then(function(){
        // $ionicScrollDelegate.scrollBottom();
        // var maxScrollableDistanceFromTop = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop;
        // console.log($ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop);
        // $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0, maxScrollableDistanceFromTop);
        // console.log(maxScrollableDistanceFromTop);
          viewScroll.scrollBottom();
          viewScroll.scrollBy(0, 9999999);



    });
    var loadCommonMatches = function(){
        $scope.commonMatches = [];
        $scope.friendMatches.forEach(function(friendMatch){
          $scope.userMatches.forEach(function(match){
            if(match.bookId === friendMatch.bookId) {
              $scope.commonMatches.push($scope.bookCache[friendMatch.bookId]);
            }
          })
        });
    };
    $scope.friendMatches.$loaded().then(loadCommonMatches);
    $scope.friendMatches.$watch(loadCommonMatches);
    $scope.userMatches.$watch(loadCommonMatches);
    $scope.bookCache.$watch();
    $scope.messages.$watch(function(e){
        if(e.event === "child_added") {
            // $ionicScrollDelegate.scrollBottom();
            // var maxScrollableDistanceFromTop = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop;
            // $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0, maxScrollableDistanceFromTop);
            viewScroll.scrollBottom();
            viewScroll.scrollBy(0, 9999999);

        }
    });
    $scope.sendMessage = function() {

        if ($scope.msg.text.length > 0) {
            $scope.messages.$add({
                from: $scope.currentUserId,
                message: $scope.msg.text,
                timestamp: Firebase.ServerValue.TIMESTAMP
            }).then(function(){
                $scope.msg.text = "";
            });
        }
        
    }
    $scope.match = function(uid) {
      var doesNotContain = function(arr, uid) {
          var x = arr;
          return x.filter(function(id){
              return id === uid;
          }).length === 0;
      };
      var chatmates = ChatMates;
      chatmates.$loaded().then(function(){
        console.log(chatmates);
          if (!chatmates.hasOwnProperty(uid)) {
              chatmates[uid] = [];
          }
          if (!chatmates.hasOwnProperty($scope.userId)) {
              chatmates[$scope.userId] = [];
          }
          if (doesNotContain(chatmates[uid], $scope.userId)) {
              chatmates[uid].push($scope.userId);
          }
          if (doesNotContain(chatmates[$scope.userId], uid)) {
              chatmates[$scope.userId].push(uid);
          }
          console.log(chatmates)
          chatmates.$save().then(function(){
              $state.go("chat", {id: uid});
          });
      });  
    }
    $scope.reject = function(uid) {
      var doesNotContain = function(arr, uid) {
          var x = arr;
          return x.filter(function(id){
              return id === uid;
          }).length === 0;
      };
      var chatmates = RejectMatch;
      chatmates.$loaded().then(function(){
          if (!chatmates.hasOwnProperty(uid)) {
              chatmates[uid] = [];
          }
          if (!chatmates.hasOwnProperty($scope.userId)) {
              chatmates[$scope.userId] = [];
          }
          if (doesNotContain(chatmates[uid], $scope.userId)) {
              chatmates[uid].push($scope.userId);
          }
          if (doesNotContain(chatmates[$scope.userId], uid)) {
              chatmates[$scope.userId].push(uid);
          }
          chatmates.$save().then(function(){
              $ionicHistory.goBack();
          });
      });  
    }
     $scope.$on('taResize', function(e, ta) {
      console.log('taResize');
      if (!ta) return;
      
      var taHeight = ta[0].offsetHeight;
      console.log('taHeight: ' + taHeight);
      
      if (!footerBar) return;
      
      var newFooterHeight = taHeight + 10;
      newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
      
      footerBar.style.height = newFooterHeight + 'px';
      scroller.style.bottom = newFooterHeight + 'px'; 
    });
})
.controller("ChatListCtrl", function($scope, Auth, ChatList, Profiles, Messages){
    $scope.loggedIn = false;
    $scope.auth = Auth;
    $scope.auth.$onAuth(function(authData) {
      if (authData) {
        $scope.loggedIn = true;
      } 
      $scope.chatmates = ChatList.get(Auth.$getAuth().facebook.id);
     
    });
    $scope.profiles = Profiles;
})
