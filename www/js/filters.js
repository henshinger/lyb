angular.module('ionicApp.filters', [])
.filter("removeOffers", function(){
    return function(books, offerArr, user_id) {
        var offers = offerArr[0].concat(offerArr[1]),
            bookIds = offers.map(function(offer){
                if (offer.uid1 === user_id) {
                    return offer.book2.id;
                }
                return offer.book1.id;
            }),
            filteredBooks = books.filter(function(book){
                return (bookIds.filter(function(bookId){
                    return bookId === book.id;
                }).length === 0);
            });
            return filteredBooks;
    };
}).filter("noZero", function(){
    return function(input){
        return input.filter(function(number){
            return number > 0;
        })
    };
}).filter("noUserMatch", function(){
    return function(matches, userId) {
        var filteredMatches = matches.filter(function(match){
            return match.uid !== userId; 
        });
        return filteredMatches;
    }
}).filter('displayIDorName', function(){
    return function(input, userId, profiles) {
        return input === userId ?  profiles[input].displayName : input;
    }
}).filter('userMatchWithDetails', function($q){
    return function(input) {
        var deferredMap = input.map(function(match){
        var dfd = $q.defer()
        gapi.client.books.volumes.get({volumeId: match.bookId}).execute(function(item){
           dfd.resolve({
                id: item.id,
                name: item.volumeInfo.title,
                description: item.volumeInfo.description || '',
                imageLinks: item.volumeInfo.imageLinks || {thumbnail:"http://placehold.it/250x250"},
                categories: item.volumeInfo.categories || [],
                authors: item.volumeInfo.authors || []
            });

        });
        return dfd.promise;
      });
      return $q.all(deferredMap).then(function(values){
        return values;
      });
    }
}).filter('userBookMatches', function(Matches){
    return function(input, userId) {
        var userObj = {};
        input.forEach(function(book){
            var obj = book;
            var matches = Matches.bookMatches(book.id);
            matches.$loaded().then(function(){
                matches.forEach(function(match){
                    if(match.uid != userId){
                        if (!userObj.hasOwnProperty(match.uid)) {
                            userObj[match.uid] = [];
                        }
                        userObj[match.uid].push(obj);    
                    }
                    
                });  
            })
              // .filter(function(matches){
              //   return matches.uid !== $scope.userId;
              // })
        });
        return userObj
    };
})
.filter('nl2br', ['$filter',
  function($filter) {
    return function(data) {
      if (!data) return data;
      return data.replace(/\n\r?/g, '<br />');
    };
  }
])
.filter('profileImageURL', function(){
    return function(userID) {
        return userID ? "http://graph.facebook.com/" + userID + "/picture?type=normal" : 'img/anon.png';
    }
});