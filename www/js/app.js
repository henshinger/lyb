angular.module('ionicApp', ['ionic', 'ionicApp.controllers', "ionicApp.directives", "ionic.contrib.ui.tinderCards", "firebase", "ionicApp.services", "ionicApp.filters", "angularMoment"])
.run(["$rootScope", "$state", function($rootScope, $state, $ionicLoading) {
    $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
      // We can catch the error thrown when the $requireAuth promise is rejected
      // and redirect the user back to the home page
      if (error === "AUTH_REQUIRED") {
        $state.go("login");
      }
    });
    $rootScope.$on('$stateNotFound',
    function(event, unfoundState, fromState, fromParams){
        console.log(unfoundState.to); // "lazy.state"
        console.log(unfoundState.toParams); // {a:1, b:2}
        console.log(unfoundState.options); // {inherit:false} + default options
    })
}])
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('login', {
        url: "/login",
        templateUrl: "templates/login.html",
        controller: "LoginCtrl",
        resolve: {
        // controller will not be loaded until $waitForAuth resolves
          // Auth refers to our $firebaseAuth wrapper in the example above
          "currentAuth": ["Auth", function(Auth) {
            // $waitForAuth returns a promise so the resolve waits for it to complete
            return Auth.$waitForAuth();
          }]
        }
    })
    .state('chat', {
      url: '/chat/:id',
      templateUrl: 'templates/chat.html',
      controller: "ChatCtrl",
      resolve: {
          "currentAuth": ["Auth", function(Auth) {
            // $requireAuth returns a promise so the resolve waits for it to complete
            // If the promise is rejected, it will throw a $stateChangeError (see above)
            return Auth.$requireAuth();
          }]
      }
    })
    .state('location', {
      url: '/location',
      templateUrl: 'templates/location.html',
      controller: "LocationCtrl",
      resolve: {
          "currentAuth": ["Auth", function(Auth) {
            // $requireAuth returns a promise so the resolve waits for it to complete
            // If the promise is rejected, it will throw a $stateChangeError (see above)
            return Auth.$requireAuth();
          }]
      }
    })
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      resolve: {
          "currentAuth": ["Auth", function(Auth) {
            // $requireAuth returns a promise so the resolve waits for it to complete
            // If the promise is rejected, it will throw a $stateChangeError (see above)
            return Auth.$requireAuth();
          }]
      }
    })
    .state('tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "templates/home.html",
          controller: 'HomeTabCtrl'
        }
      }
    })
    .state('tabs.offers', {
      url: "/offers",
      views: {
        'offers-tab': {
          templateUrl: "templates/offers.html",
          controller: 'OffersTabCtrl'
        }
      }
    })
    .state('tabs.user', {
      url: "/user",
      views: {
        'user-tab': {
          templateUrl: "templates/user.html",
          controller: 'UserTabCtrl'
        }
      }
    })
    .state('tabs.friends', {
      url: "/friends",
      views: {
        'friends-tab': {
          templateUrl: "templates/friends.html",
          controller: 'FriendsTabCtrl'
        }
      }
    })
    .state('tabs.friend', {
      url: "/friend/:id",
      views: {
        'friends-tab': {
          templateUrl: "templates/friend.html",
          controller: 'FriendTabCtrl'
        }
      }
    })
    .state('tabs.explore', {
      url: "/explore",
      views: {
        'explore-tab': {
          templateUrl: "templates/explore.html",
          controller: 'ExploreTabCtrl'
        }
      }
    })
    .state('tabs.exploreList', {
      url: "/explore/list",
      views: {
        'explore-tab': {
          templateUrl: "templates/explorelist.html",
          controller: 'ExploreListTabCtrl'
        }
      },
      params: {
          selectedBook: null
      }
    })
    .state('tabs.mnm', {
      url: "/mnm",
      views: {
        'mnm-tab': {
          templateUrl: "templates/mnm.html",
          controller: 'MixAndMatchTabCtrl'
        }
      }
    })
    .state('tabs.match', {
      url: "/match",
      views: {
        'match-tab': {
          templateUrl: "templates/match.html",
          controller: 'MatchTabCtrl'
        }
      }
    })
    .state('tabs.matchList', {
      url: "/match/list",
      views: {
        'match-tab': {
          templateUrl: "templates/matchlist.html",
          controller: 'MatchListTabCtrl'
        }
      },
      params: {
          selectedBook: null
      }
    })
    .state('tabs.tempchat', {
      url: '/match/tempchat/:id',
      views: {
        'match-tab': {
          controller: "TempChatCtrl",
          templateUrl: 'templates/tempchat.html',    
        }
      }
    });


   $urlRouterProvider.otherwise("/login");

});

// // for ui-router


// app.config(["$stateProvider", function ($stateProvider) {
// $stateProvider
//   .state("LoginCtrl", {
//     // the rest is the same for ui-router and ngRoute...
//     controller: "HomeCtrl",
//     templateUrl: "views/home.html",
    
//   })
//   .state("account", {
//     // the rest is the same for ui-router and ngRoute...
//     controller: "AccountCtrl",
//     templateUrl: "views/account.html",
//     resolve: {
//       // controller will not be loaded until $requireAuth resolves
//       // Auth refers to our $firebaseAuth wrapper in the example above
//       "currentAuth": ["Auth", function(Auth) {
//         // $requireAuth returns a promise so the resolve waits for it to complete
//         // If the promise is rejected, it will throw a $stateChangeError (see above)
//         return Auth.$requireAuth();
//       }]
//     }
//   });
// }]);
