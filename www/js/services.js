angular.module('ionicApp.services', [])

.factory("Auth", function($firebaseAuth) {
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/");
  return $firebaseAuth(usersRef);
})
.factory("Profiles", function($firebaseObject){
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/profiles");
  return $firebaseObject(usersRef);
})
.factory("BookCache", function($firebaseObject){
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/bookcache");
  return $firebaseObject(usersRef);
})


.factory("BooksArrayFactory", function($firebaseAuth, $firebaseArray, Auth, $q){
    
    var user_id = Auth.$getAuth().facebook.id;
    return $firebaseArray.$extend({
        getUserBooks:function(){
          var deferred = $q.defer();
          // query by 'name'
          this.$ref().orderByChild("user_id").equalTo(user_id).once("value", function(dataSnapshot){
            if(dataSnapshot.exists()){
              deferred.resolve(dataSnapshot.val());
            } else {
              deferred.reject("Not found.");
            }
          });
          return deferred.promise;
        }
      });
})
.factory("BooksFactory", function(BooksArrayFactory){
    var booksRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/books");
    return new BooksArrayFactory(booksRef);
})
.factory("Books", function($firebaseArray, Auth, Profiles, Offers){
    var booksRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/books");
    return {
        all: function() {
            return $firebaseArray(booksRef);
        }, 
        userBooks: function() {
            var user_id = Auth.$getAuth().facebook.id,
                query = booksRef.orderByChild("user_id").equalTo(user_id);
            return $firebaseArray(query);
        },
        searchBooks: function(book) {
            var offers = Offers.userOffers();
            offers[1].$loaded().then(function(){
                offers = offers[0].concat(offers[1]);
                offers = offers.map(function(offer){
                    return [offer.ref1, offer.ref2];
                });
                offers = [].concat.apply([], offers);
            });
            return book.tradeBooksList.map(function(tradeBook){
                var query = booksRef.orderByChild("id").equalTo(tradeBook.id);
                tradeBook.query = $firebaseArray(query);
                tradeBook.query.$loaded().then(function(){
                    tradeBook.query = tradeBook.query.filter(function(queryBook){
                        return offers.indexOf(tradeBook.query.$keyAt(queryBook)) === -1;
                    }).map(function(queryBook){
                        queryBook.ref = tradeBook.query.$keyAt(queryBook);
                        queryBook.user = Profiles[queryBook.user_id];
                        return queryBook;
                    });    
                });
                
                return tradeBook;
            });
        }
    };
})
.factory("Messages", function($firebaseArray){
  var messagesRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/messages");
  return {
      forUsers: function(uid1, uid2){
        var path = uid1 < uid2 ? uid1+'/'+uid2 : uid2+'/'+uid1;
    
        return $firebaseArray(messagesRef.child(path));
      },
      lastMessage: function(uid1, uid2){
        var path = uid1 < uid2 ? uid1+'/'+uid2 : uid2+'/'+uid1;
    
        return $firebaseArray(messagesRef.child(path).limitToLast(1));
      }
  }; 
})
.factory("Offers", function($firebaseArray, $firebaseObject, Auth){
  var messagesRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/offers");
  return {
      forUsers: function(uid1, uid2){
        var path = uid1 < uid2 ? uid1+'/'+uid2 : uid2+'/'+uid1;
    
        return $firebaseArray(messagesRef.child(path));
      },
      all: function() {
          return $firebaseArray(messagesRef);
      },
      userOffers: function() {
        var user_id = Auth.$getAuth().facebook.id,
            query1 = messagesRef.orderByChild("uid1").equalTo(user_id),
            query2 = messagesRef.orderByChild("uid2").equalTo(user_id),
            query = [$firebaseArray(query1), $firebaseArray(query2)];
            
        return query;
      },
      get: function(ref) {
          var messagesRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/offers/" + ref);
          return $firebaseObject(messagesRef);
      }
  }; 
})
.factory("Matches", function($firebaseArray, $firebaseObject, Auth){
  var matchesRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/matches");
  return {
      all: function() {
          return $firebaseArray(matchesRef);
      },
      userMatches: function() {
        var user_id = Auth.$getAuth().facebook.id,
            query = matchesRef.orderByChild("uid").equalTo(user_id);
        return $firebaseArray(query);
      },
      bookMatches: function(bookId) {
        var query = matchesRef.orderByChild("bookId").equalTo(bookId);
        return $firebaseArray(query);
      },
      friendMatches: function(user_id) {
        var query = matchesRef.orderByChild("uid").equalTo(user_id);
        return $firebaseArray(query);
      }
  }; 
})
.factory("ChatMates", function($firebaseObject){
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/chatmates");
  return $firebaseObject(usersRef);
})
.factory("TempMatch", function($firebaseObject){
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/tempmatch");
  return $firebaseObject(usersRef);
})
.factory("RejectMatch", function($firebaseObject){
  var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/tempmatch");
  return $firebaseObject(usersRef);
})
.factory("ChatList", function($firebaseObject){
    return {
        get: function(id) {
            var usersRef = new Firebase("https//fiery-heat-4329.firebaseIO.com/chatmates/" + id);
            return $firebaseObject(usersRef);
        }
    };
})
.factory("Places", function(){
    var MASTERLIST = [
            "Katipunan",
            "Binondo",
            "LRT2"
        ];
    return {
        filter: function(q) {
            return MASTERLIST.filter(function(place){
                return place.toLowerCase().indexOf(q.toLowerCase()) > -1;
            }).concat([q]);
        }
    }
})
.factory('Camera', ['$q', function($q) {

  return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }
}])

.service("$focusTest", function(){

  this.focusOnBlur = true;

  this.setFocusOnBlur = function(val){
      this.focusOnBlur = val;
  }

  this.getFocusOnBlur = function(){
      return this.focusOnBlur;
  }
});